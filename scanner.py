#!/usr/bin/env python
from scapy.all import *
import sqlite3 as lite

con = lite.connect('clients.db')
interface = "mon0"
PROBE_REQUEST_TYPE=0
PROBE_REQUEST_SUBTYPE=4

def handlePackage(pkt):
	if pkt.haslayer(Dot11):
        	if pkt.type==PROBE_REQUEST_TYPE and pkt.subtype == PROBE_REQUEST_SUBTYPE:
			#Paket ist von Wifi und ein Probe-Request
			try:
	        		extra = pkt.notdecoded
    			except:
        			extra = None
    			if extra!=None:
        			signal_strength = -(256-ord(extra[-4:-3]))
    			else:
        			signal_strength = -100

			params = (pkt.addr2, signal_strength)
			con.execute("INSERT INTO clients (mac, strength) VALUES(?, ?);", params)
			con.commit()

sniff(iface=interface, prn=handlePackage, store=0)
